import { defineNuxtPlugin } from "#app";
import translations from '../translations/en.js';


export default defineNuxtPlugin((nuxtApp) => {
    nuxtApp.vueApp.use({
        install(app, options) {
            app.config.globalProperties.$t = (key, params = {}) => {
                let value = translations;
                for (let part of key.split('.')) {
                    value = value[part];
                    if (value === undefined) {
                        console.error('Cannot find translation: ' + key);
                        return undefined;
                    }
                }

                for (let k in params) {
                    if (params.hasOwnProperty(k)) {
                        value = Array.isArray(value)
                            ? value.map(v => v.replace(new RegExp('%' + k + '%', 'g'), params[k]))
                            : value.replace(new RegExp('%' + k + '%', 'g'), params[k]);
                    }
                }

                return value;
            }
        }
    });
});
