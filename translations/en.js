export default {
    title: 'Emoji Flags',
    about:
        'Only two pride flags have their own emoji. For all the others we need to resort to using colourful hearts and symbols. ' +
        'With this tool you can just look up a flag and copy-paste those symbols. ' +
        'Country flags are available too! ' +
        'You can also use a keyboard to compose a flag yourself.'
    ,
    keywords: 'flags, emoji, keyboard, pride, countries, hearts',
    menu: 'Menu',
    fallbacks:
        'Keep in mind that this representation is just a simplification. ' +
        'Not all the colours are available, ' +
        'and for the basic ones, not all have a corresponding emoji heart, circle or square. ' +
        'For pink we need to fall back to using red, and for grey we must use brown instead. ' +
        'You can manually override our choices for each colour.'
    ,
    flag: 'Flag',
    groups: {
        pride: {
            name: 'Pride',
            title: 'Emoji Pride Flags',
            values: {
                Abroromantic: 'Abroromantic',
                Abrosexual: 'Abrosexual',
                Achillean: 'Achillean',
                Agender: 'Agender',
                Alloromantic_Asexual: 'Alloromantic Asexual',
                Ambiamorous: 'Ambiamorous',
                Ambiamorous_: 'Ambiamorous',
                Anarcha_Queer: 'Anarcha-Queer',
                Androgyne: 'Androgyne',
                Androsexual: 'Androsexual',
                Aporagender: 'Aporagender',
                Archaeopronouns: 'Archaeopronouns',
                Aroace: 'Aroace',
                Aromantic: 'Aromantic',
                Aromantic_Allosexual: 'Aromantic Allosexual',
                Asexual: 'Asexual',
                Autigender: 'Autigender',
                Bear: 'Bear',
                Bicurious: 'Bicurious',
                Bigender: 'Bigender',
                Bigender_: 'Bigender',
                Biromantic: 'Biromantic',
                Bisexual: 'Bisexual',
                Butch: 'Butch',
                Ceteroromantic: 'Ceteroromantic',
                Ceterosexual: 'Ceterosexual',
                Cis_Ally: 'Cis Ally',
                Chastity_Pride: 'Chastity Pride / TeamLocked',
                Demiboy: 'Demiboy',
                Demigender: 'Demigender',
                Demigirl: 'Demigirl',
                Demiromantic: 'Demiromantic',
                Demisexual: 'Demisexual',
                Diamoric: 'Diamoric',
                Drag: 'Drag',
                Enbian: 'Enbian',
                Faafafine: 'Fa\'afafine',
                Femme: 'Femme',
                Gay: 'Gay',
                Genderfae: 'Genderfae, Genderdoe',
                Genderfaun: 'Genderfaun',
                Genderfluid: 'Genderfluid',
                Genderflux: 'Genderflux',
                Genderqueer: 'Genderqueer',
                Greyaromantic: 'Greyaromantic',
                Greyasexual: 'Greyasexual',
                Gynesexual: 'Gynesexual',
                Heteroflexible: 'Heteroflexible',
                Heteroromantic: 'Heteroromantic',
                Heterosexual: 'Heterosexual',
                Hijra: 'Hijra',
                Homoflexible: 'Homoflexible',
                Homoromantic: 'Homoromantic',
                Intersex: 'Intersex',
                Leather_Pride: 'Leather Pride',
                Lesbian: 'Lesbian',
                Lesbian_: 'Lesbian',
                Lesbiromantic: 'Lesbiromantic',
                Maverique: 'Maverique',
                Monoamorous: 'Monoamorous, Monogamous',
                Muxe: 'Muxe',
                Nebularomantic: 'Nebularomantic',
                Neopronouns: 'Neopronouns',
                Neopronouns_: 'Neopronouns',
                Neutrois: 'Neutrois',
                Nonbinary: 'Nonbinary',
                Omniromantic: 'Omniromantic',
                Omnisexual: 'Omnisexual',
                Oriented_Aroace: 'Oriented Aroace',
                Pangender: 'Pangender',
                Panromantic: 'Panromantic',
                Pansexual: 'Pansexual',
                Polyamorous: 'Polyamorous',
                Polyamorous_: 'Polyamorous',
                Polyamorous__: 'Polyamorous',
                Polyromantic: 'Polyromantic',
                Polysexual: 'Polysexual',
                Pomoromantic: 'Pomoromantic',
                Pomosexual: 'Pomosexual',
                Progress_Pride: 'Progress Pride',
                Queer: 'Queer',
                Queerian: 'Queerian',
                Queerplatonic: 'Queerplatonic',
                Quoiromantic: 'Quoiromantic',
                Rainbow: 'Rainbow, Gay, Homosexual, Queer, LGBTQ+',
                Sapphic: 'Sapphic',
                Sapphic_: 'Sapphic',
                Straight_Ally: 'Straight Ally',
                Toric: 'Toric',
                Transfeminine: 'Transfeminine',
                Transgender: 'Transgender',
                Transmasculine: 'Transmasculine',
                Transneutral: 'Transneutral',
                Trigender: 'Trigender',
                Trixic: 'Trixic',
                Two_Spirit: 'Two Spirit',
                Xenogender: 'Xenogender',
            }
        },
        country: {
            name: 'Country',
            title: 'Emoji Country Flags',
            values: {
                AF: 'Afghanistan',
                AX: 'Åland Islands',
                AL: 'Albania',
                DZ: 'Algeria',
                AS: 'American Samoa',
                AD: 'Andorra',
                AO: 'Angola',
                AI: 'Anguilla',
                AQ: 'Antarctica',
                AG: 'Antigua and Barbuda',
                AR: 'Argentina',
                AM: 'Armenia',
                AW: 'Aruba',
                AU: 'Australia',
                AT: 'Austria',
                AZ: 'Azerbaijan',
                BS: 'Bahamas',
                BH: 'Bahrain',
                BD: 'Bangladesh',
                BB: 'Barbados',
                BY: 'Belarus',
                BE: 'Belgium',
                BZ: 'Belize',
                BJ: 'Benin',
                BM: 'Bermuda',
                BT: 'Bhutan',
                BO: 'Bolivia',
                BQ: 'Bonaire, Sint Eustatius and Saba',
                BA: 'Bosnia and Herzegovina',
                BW: 'Botswana',
                BR: 'Brazil',
                IO: 'British Indian Ocean Territory',
                BN: 'Brunei Darussalam',
                BG: 'Bulgaria',
                BF: 'Burkina Faso',
                BI: 'Burundi',
                KH: 'Cambodia',
                CM: 'Cameroon',
                CA: 'Canada',
                CV: 'Cape Verde',
                KY: 'Cayman Islands',
                CF: 'Central African Republic',
                TD: 'Chad',
                CL: 'Chile',
                CN: 'China',
                CX: 'Christmas Island',
                CC: 'Cocos (Keeling) Islands',
                CO: 'Colombia',
                KM: 'Comoros',
                CG: 'Congo',
                CD: 'Congo, The Democratic Republic of the',
                CK: 'Cook Islands',
                CR: 'Costa Rica',
                HR: 'Croatia',
                CU: 'Cuba',
                CW: 'Curaçao',
                CY: 'Cyprus',
                CZ: 'Czechia',
                CI: "Côte d'Ivoire",
                DK: 'Denmark',
                DJ: 'Djibouti',
                DM: 'Dominica',
                DO: 'Dominican Republic',
                EC: 'Ecuador',
                EG: 'Egypt',
                SV: 'El Salvador',
                GQ: 'Equatorial Guinea',
                ER: 'Eritrea',
                EE: 'Estonia',
                SZ: 'Eswatini',
                ET: 'Ethiopia',
                EU: 'Europe',
                FK: 'Falkland Islands (Malvinas)',
                FO: 'Faroe Islands',
                FJ: 'Fiji',
                FI: 'Finland',
                FR: 'France',
                GF: 'French Guiana',
                PF: 'French Polynesia',
                TF: 'French Southern Territories',
                GA: 'Gabon',
                GM: 'Gambia',
                GE: 'Georgia',
                DE: 'Germany',
                GH: 'Ghana',
                GI: 'Gibraltar',
                GR: 'Greece',
                GL: 'Greenland',
                GD: 'Grenada',
                GP: 'Guadeloupe',
                GU: 'Guam',
                GT: 'Guatemala',
                GG: 'Guernsey',
                GN: 'Guinea',
                GW: 'Guinea-Bissau',
                GY: 'Guyana',
                HT: 'Haiti',
                HM: 'Heard Island and McDonald Islands',
                VA: 'Holy See (Vatican City State)',
                HN: 'Honduras',
                HK: 'Hong Kong',
                HU: 'Hungary',
                IS: 'Iceland',
                IN: 'India',
                ID: 'Indonesia',
                IR: 'Iran, Islamic Republic of',
                IQ: 'Iraq',
                IE: 'Ireland',
                IM: 'Isle of Man',
                IL: 'Israel',
                IT: 'Italy',
                JM: 'Jamaica',
                JP: 'Japan',
                JE: 'Jersey',
                JO: 'Jordan',
                KZ: 'Kazakhstan',
                KE: 'Kenya',
                KI: 'Kiribati',
                KP: "Korea, Democratic People's Republic of",
                KR: 'Korea, Republic of',
                XK: 'Kosovo',
                KW: 'Kuwait',
                KG: 'Kyrgyzstan',
                LA: "Lao People's Democratic Republic",
                LV: 'Latvia',
                LB: 'Lebanon',
                LS: 'Lesotho',
                LR: 'Liberia',
                LY: 'Libya',
                LI: 'Liechtenstein',
                LT: 'Lithuania',
                LU: 'Luxembourg',
                MO: 'Macao',
                MG: 'Madagascar',
                MW: 'Malawi',
                MY: 'Malaysia',
                MV: 'Maldives',
                ML: 'Mali',
                MT: 'Malta',
                MH: 'Marshall Islands',
                MQ: 'Martinique',
                MR: 'Mauritania',
                MU: 'Mauritius',
                YT: 'Mayotte',
                MX: 'Mexico',
                FM: 'Micronesia, Federated States of',
                MD: 'Moldova, Republic of',
                MC: 'Monaco',
                MN: 'Mongolia',
                ME: 'Montenegro',
                MS: 'Montserrat',
                MA: 'Morocco',
                MZ: 'Mozambique',
                MM: 'Myanmar',
                NA: 'Namibia',
                NR: 'Nauru',
                NP: 'Nepal',
                NL: 'Netherlands',
                NC: 'New Caledonia',
                NZ: 'New Zealand',
                NI: 'Nicaragua',
                NE: 'Niger',
                NG: 'Nigeria',
                NU: 'Niue',
                NF: 'Norfolk Island',
                MK: 'North Macedonia',
                MP: 'Northern Mariana Islands',
                NO: 'Norway',
                OM: 'Oman',
                PK: 'Pakistan',
                PW: 'Palau',
                PS: 'Palestine, State of',
                PA: 'Panama',
                PG: 'Papua New Guinea',
                PY: 'Paraguay',
                PE: 'Peru',
                PH: 'Philippines',
                PN: 'Pitcairn',
                PL: 'Poland',
                PT: 'Portugal',
                PR: 'Puerto Rico',
                QA: 'Qatar',
                RE: 'Reunion',
                RO: 'Romania',
                RU: 'Russia',
                RW: 'Rwanda',
                BL: 'Saint Barthélemy',
                SH: 'Saint Helena, Ascension and Tristan Da Cunha',
                KN: 'Saint Kitts and Nevis',
                LC: 'Saint Lucia',
                MF: 'Saint Martin (French Part)',
                PM: 'Saint Pierre and Miquelon',
                VC: 'Saint Vincent and the Grenadines',
                WS: 'Samoa',
                SM: 'San Marino',
                ST: 'Sao Tome and Principe',
                SA: 'Saudi Arabia',
                SN: 'Senegal',
                RS: 'Serbia',
                SC: 'Seychelles',
                SL: 'Sierra Leone',
                SG: 'Singapore',
                SX: 'Sint Maarten (Dutch Part)',
                SK: 'Slovakia',
                SI: 'Slovenia',
                SB: 'Solomon Islands',
                SO: 'Somalia',
                ZA: 'South Africa',
                GS: 'South Georgia and the South Sandwich Islands',
                SS: 'South Sudan',
                ES: 'Spain',
                LK: 'Sri Lanka',
                SD: 'Sudan',
                SR: 'Suriname',
                SJ: 'Svalbard and Jan Mayen',
                SE: 'Sweden',
                CH: 'Switzerland',
                SY: 'Syrian Arab Republic',
                TW: 'Taiwan',
                TJ: 'Tajikistan',
                TZ: 'Tanzania, United Republic of',
                TH: 'Thailand',
                TL: 'Timor-Leste',
                TG: 'Togo',
                TK: 'Tokelau',
                TO: 'Tonga',
                TT: 'Trinidad and Tobago',
                TN: 'Tunisia',
                TR: 'Turkey',
                TM: 'Turkmenistan',
                TC: 'Turks and Caicos Islands',
                TV: 'Tuvalu',
                UG: 'Uganda',
                UA: 'Ukraine',
                AE: 'United Arab Emirates',
                GB: 'United Kingdom',
                US: 'United States',
                UY: 'Uruguay',
                UZ: 'Uzbekistan',
                VU: 'Vanuatu',
                VE: 'Venezuela',
                VN: 'Viet Nam',
                VG: 'Virgin Islands, British',
                VI: 'Virgin Islands, U.S.',
                WF: 'Wallis and Futuna',
                EH: 'Western Sahara',
                YE: 'Yemen',
                ZM: 'Zambia',
                ZW: 'Zimbabwe',
            }
        },
        keyboard: {
            name: 'Keyboard',
            title: 'Emoji Flag Keyboard',
        },
    },
    types: {
        hearts: 'Hearts',
        squares: 'Squares',
        circles: 'Circles',
        countryMarkers: 'Country markers',
        special: 'Special characters',
    },
    settings: {
        addSpaces: {
            true: 'With spaces',
            false: 'No spaces',
        },
        twemoji: {
            true: 'Twitter emoji',
            false: 'System emoji',
        },
        override: 'Override colours',
        filter: 'Filter',
    },
    colours: {
        pink: 'Pink',
        red: 'Red',
        orange: 'Orange',
        yellow: 'Yellow',
        green: 'Green',
        blue: 'Blue',
        purple: 'Purple',
        brown: 'Brown',
        black: 'Black',
        grey: 'Grey',
        white: 'White',
    },
    keyboard: {
        placeholder: 'Click on the symbols above. The result will show up here.',
    }
};
