export const C = {
    pink: ['pink', 'red'],
    red: ['red'],
    orange: ['orange'],
    yellow: ['yellow'],
    green: ['green'],
    blue: ['blue'],
    purple: ['purple'],
    brown: ['brown'],
    black: ['black'],
    grey: ['grey', 'brown'],
    white: ['white'],
}

export const hearts = {
    pink: '🩷',
    red: '❤️',
    orange: '🧡',
    yellow: '💛',
    green: '💚',
    lightblue: '🩵',
    blue: '💙',
    purple: '💜',
    brown: '🤎',
    black: '🖤',
    grey: '🩶',
    white: '🤍',
};

export const squares = {
    red: '🟥',
    orange: '🟧',
    yellow: '🟨',
    green: '🟩',
    blue: '🟦',
    purple: '🟪',
    brown: '🟫',
    black: '⬛️',
    white: '⬜️',
};

export const circles = {
    red: '🔴',
    orange: '🟠',
    yellow: '🟡',
    green: '🟢',
    blue: '🔵',
    purple: '🟣',
    brown: '🟤',
    black: '⚫️',
    white: '⚪️',
};

export const countryMarkers = {
    'A': '🇦', 'B': '🇧', 'C': '🇨', 'D': '🇩', 'E': '🇪', 'F': '🇫', 'G': '🇬', 'H': '🇭', 'I': '🇮', 'J': '🇯',
    'K': '🇰', 'L': '🇱', 'M': '🇲', 'N': '🇳', 'O': '🇴', 'P': '🇵', 'Q': '🇶', 'R': '🇷', 'S': '🇸', 'T': '🇹',
    'U': '🇺', 'V': '🇻', 'W': '🇼', 'X': '🇽', 'Y': '🇾', 'Z': '🇿',
};

export const types = {
    hearts,
    squares,
    circles,
    countryMarkers,
};

class Flag {
    constructor(key, colours, specials = [], source = null) {
        this._key = key;
        this._colours = colours;
        this._specials = specials;
        this._source = source;
    }

    key() {
        return this._key;
    }

    emojiString(type, addSpaces, overrides = {}) {
        return this._colours.map(colour => {
            if (!Array.isArray(colour)) {
                return colour;
            }
            for (let fallback of colour) {
                if (overrides[fallback] !== undefined) {
                    return overrides[fallback];
                }
                let val = types[type][fallback];
                if (val !== undefined) {
                    return val;
                }
            }
        }).join(addSpaces ? ' ' : '');
    }

    specials() {
        return this._specials;
    }

    source() {
        return this._source;
    }

    image() {
        return null;
    }
}

class CountryFlag extends Flag {
    specials() {
        return [[
            this.image(),
        ]];
    }

    image() {
        return countryMarkers[this._key[0]] + countryMarkers[this._key[1]];
    }
}

class FlagGroup {
    constructor(flags) {
        flags.sort((a, b) => a.key().localeCompare(b.key()));
        this._flags = {};
        for (let flag of flags) {
            this._flags[flag.key()] = flag;
        }
    }

    all() {
        return Object.values(this._flags);
    }

    get(key) {
        return this._flags[key];
    }
}

export const flags = {
    pride: new FlagGroup([
        new Flag('Abroromantic', [C.green, C.white, C.red]),
        new Flag('Abrosexual', [C.green, C.white, C.red]),
        new Flag('Achillean', [C.blue, C.white, C.blue]),
        new Flag('Agender', [C.black, C.white, C.green, C.white, C.black]),
        new Flag('Alloromantic_Asexual', [C.red, C.pink, C.white, C.purple]),
        new Flag('Ambiamorous', [C.purple, C.black, C.red], [['α']]),
        new Flag('Ambiamorous_', [C.blue, C.black, C.red], [['α']]),
        new Flag('Anarcha_Queer', [C.pink, C.black]),
        new Flag('Androgyne', [C.pink, C.purple, C.black]),
        new Flag('Androsexual', [C.blue, C.brown, C.purple]),
        new Flag('Aporagender', [C.pink, C.yellow, C.blue]),
        new Flag('Archaeopronouns', [C.grey, C.white, C.yellow]),
        new Flag('Aroace', [C.orange, C.yellow, C.white, C.blue, C.purple], [['♠']]),
        new Flag('Aromantic', [C.green, C.white, C.grey, C.black]),
        new Flag('Aromantic_Allosexual', [C.green, C.white, C.yellow]),
        new Flag('Asexual', [C.black, C.grey, C.white, C.purple], [['♥']]),
        new Flag('Autigender', [C.green, C.black], [['∞']]),
        new Flag('Bear', [C.brown, C.orange, C.yellow, C.white, C.black], [['🐾'], ['🐻']]),
        new Flag('Bicurious', [C.pink, C.white, C.blue]),
        new Flag('Bigender', [C.pink, C.white, C.blue]),
        new Flag('Bigender_', [C.pink, C.yellow, C.white, C.purple, C.blue]),
        new Flag('Biromantic', [C.pink, C.purple, C.blue]),
        new Flag('Bisexual', [C.pink, C.purple, C.blue]),
        new Flag('Butch', [C.grey, C.white, C.purple]),
        new Flag('Ceteroromantic', [C.yellow, C.green, C.white, C.black]),
        new Flag('Ceterosexual', [C.yellow, C.green, C.white, C.black]),
        new Flag('Chastity_Pride', [C.purple, C.blue, C.white, C.blue, C.purple], [['🔐'], ['🔒']]),
        new Flag('Cis_Ally', [C.blue, C.pink, C.white, C.pink, C.blue, '▲'], [['🏳️‍⚧️', '▲']]),
        new Flag('Demiboy', [C.grey, C.blue, C.white, C.blue, C.grey]),
        new Flag('Demigender', [C.grey, C.yellow, C.white, C.yellow, C.grey]),
        new Flag('Demigirl', [C.grey, C.pink, C.white, C.pink, C.grey]),
        new Flag('Demiromantic', [C.black, C.white, C.green, C.grey], [['♦'], ['▶', '⬜️', '🟩', '🟫']]),
        new Flag('Demisexual', [C.black, C.white, C.purple, C.grey], [['♦'], ['▶', '⬜️', '🟪', '🟫']]),
        new Flag('Diamoric', [C.green, C.white, C.green]),
        new Flag('Drag', [C.purple, C.white, C.blue], [['👑']]),
        new Flag('Enbian', [C.black, C.purple], [['🌘']]),
        new Flag('Faafafine', [C.red, C.yellow, C.red]),
        new Flag('Femme', [C.purple, C.pink, C.white, C.pink, C.purple]),
        new Flag('Gay', [C.green, C.white, C.blue]),
        new Flag('Genderfae', [C.green, C.yellow, C.white, C.pink, C.purple]),
        new Flag('Genderfaun', [C.orange, C.yellow, C.white, C.grey, C.blue]),
        new Flag('Genderfluid', [C.pink, C.white, C.purple, C.black, C.blue]),
        new Flag('Genderflux', [C.pink, C.grey, C.blue, C.yellow]),
        new Flag('Genderqueer', [C.purple, C.white, C.green]),
        new Flag('Greyaromantic', [C.green, C.grey, C.white, C.grey, C.green], [['♣']]),
        new Flag('Greyasexual', [C.purple, C.grey, C.white, C.grey, C.purple], [['♣']]),
        new Flag('Gynesexual', [C.pink, C.brown, C.green]),
        new Flag('Heteroflexible', [C.blue, C.white, C.pink, '|', C.red, C.orange, C.yellow, C.green, C.blue, C.purple]),
        new Flag('Heteroromantic', [C.blue, C.white, C.pink]),
        new Flag('Heterosexual', [C.blue, C.white, C.pink]),
        new Flag('Hijra', [C.pink, C.white, C.red, C.white, C.blue]),
        new Flag('Homoflexible', [C.red, C.orange, C.yellow, C.green, C.blue, C.purple, '|', C.blue, C.white, C.pink]),
        new Flag('Homoromantic', [C.red, C.orange, C.yellow, C.green, C.blue, C.purple], [['🏳️‍🌈']]),
        new Flag('Intersex', [C.yellow, C.purple, C.yellow], [['🟨','🟣','🟨']]),
        new Flag('Leather_Pride', [C.red, C.black, C.blue, C.white, C.blue, C.black]),
        new Flag('Lesbian', [C.red, C.white, C.pink]),
        new Flag('Lesbian_', [C.purple, C.pink, C.yellow, C.green]),
        new Flag('Lesbiromantic', [C.red, C.white, C.pink]),
        new Flag('Maverique', [C.yellow, C.white, C.orange]),
        new Flag('Monoamorous', [C.white, C.black], [['µ']]),
        new Flag('Muxe', [C.black, C.white, C.green, C.red]),
        new Flag('Nebularomantic', [C.red, C.pink, C.white, C.blue, C.black]),
        new Flag('Neopronouns', [C.green, C.white, C.blue]),
        new Flag('Neopronouns_', [C.green, C.blue, C.white, C.yellow, C.orange]),
        new Flag('Neutrois', [C.white, C.green, C.black]),
        new Flag('Nonbinary', [C.yellow, C.white, C.purple, C.black]),
        new Flag('Omniromantic', [C.pink, C.black, C.blue]),
        new Flag('Omnisexual', [C.pink, C.black, C.blue]),
        new Flag('Oriented_Aroace', [C.black, C.grey, C.white, C.green]),
        new Flag('Pangender', [C.yellow, C.pink, C.white, C.pink, C.yellow]),
        new Flag('Panromantic', [C.pink, C.yellow, C.blue]),
        new Flag('Pansexual', [C.pink, C.yellow, C.blue]),
        new Flag('Polyamorous', [C.red, '∞', C.red], [['∞']]),
        new Flag('Polyamorous_', [C.blue, C.red, C.black], [['π']]),
        new Flag('Polyamorous__', [C.yellow, C.blue, C.red, C.black]),
        new Flag('Polyromantic', [C.pink, C.green, C.blue]),
        new Flag('Polysexual', [C.pink, C.green, C.blue]),
        new Flag('Pomoromantic', [C.pink, C.white, C.green, C.white, C.pink]),
        new Flag('Pomosexual', [C.pink, C.white, C.pink, C.white, C.pink]),
        new Flag('Progress_Pride', [C.red, C.orange, C.yellow, C.green, C.blue, C.purple, C.brown, C.black]),
        new Flag('Queer', [C.pink, C.purple]),
        new Flag('Queerian', [C.pink, C.purple, C.black]),
        new Flag('Queerplatonic', [C.yellow, C.pink, C.white, C.grey, C.black]),
        new Flag('Quoiromantic', [C.black, C.green, C.blue, C.grey]),
        new Flag('Rainbow', [C.red, C.orange, C.yellow, C.green, C.blue, C.purple], [['🏳️‍🌈']]),
        new Flag('Sapphic', [C.pink, C.white, C.pink]),
        new Flag('Sapphic_', [C.blue, C.pink, C.yellow, C.green]),
        new Flag('Straight_Ally', [C.red, C.orange, C.yellow, C.green, C.blue, C.purple, '▲'], [['🏳️‍⚧️', '▲']]),
        new Flag('Toric', [C.purple, C.pink, C.blue, C.green], [['*', '♂']]),
        new Flag('Transfeminine', [C.blue, C.pink, C.pink, C.pink, C.blue]),
        new Flag('Transgender', [C.blue, C.pink, C.white, C.pink, C.blue], [['🏳️‍⚧️']]),
        new Flag('Transmasculine', [C.pink, C.blue, C.blue, C.blue, C.pink]),
        new Flag('Transneutral', [C.blue, C.yellow, C.yellow, C.yellow, C.pink]),
        new Flag('Trigender', [C.pink, C.purple, C.green, C.purple, C.pink]),
        new Flag('Trixic', [C.purple, C.pink, C.yellow, C.orange], [['*', '♀']]),
        new Flag('Two_Spirit', [C.black, C.white, C.yellow, C.red, C.blue, C.green]),
        new Flag('Xenogender', [C.pink, C.orange, C.yellow, C.blue, C.purple]),
    ]),
    country: new FlagGroup([
        new CountryFlag('EU', [C.blue, C.yellow, C.blue]), // 🇪🇺
        new CountryFlag('AF', [C.black, C.red, C.green]), // 🇦🇫
        new CountryFlag('AX', [C.blue, C.red]), // 🇦🇽
        new CountryFlag('AL', [C.red]), // 🇦🇱
        new CountryFlag('DZ', [C.green, C.white]), // 🇩🇿
        new CountryFlag('AS', [C.blue, C.white, C.blue]), // 🇦🇸
        new CountryFlag('AD', [C.blue, C.yellow, C.red]), // 🇦🇩
        new CountryFlag('AO', [C.red, C.black]), // 🇦🇴
        new CountryFlag('AI', ['🇬🇧', C.blue]), // 🇦🇮
        new CountryFlag('AQ', [C.blue, C.white]), // 🇦🇶
        new CountryFlag('AG', [C.red, C.black, '🌅', C.blue, C.white, C.red]), // 🇦🇬
        new CountryFlag('AR', [C.blue, C.white, '🌞', C.blue]), // 🇦🇷
        new CountryFlag('AM', [C.red, C.blue, C.yellow]), // 🇦🇲
        new CountryFlag('AW', [C.blue, C.yellow]), // 🇦🇼
        new CountryFlag('AU', ['🇬🇧', C.blue, '✨']), // 🇦🇺
        new CountryFlag('AT', [C.red, C.white, C.red]), // 🇦🇹
        new CountryFlag('AZ', [C.blue, C.red, '☪', C.green]), // 🇦🇿
        new CountryFlag('BS', [C.black, C.blue, C.yellow, C.blue]), // 🇧🇸
        new CountryFlag('BH', [C.white, C.red]), // 🇧🇭
        new CountryFlag('BD', [C.green, C.red]), // 🇧🇩
        new CountryFlag('BB', [C.blue, C.yellow, C.blue]), // 🇧🇧
        new CountryFlag('BY', [C.white, C.red, C.white]), // 🇧🇾
        new CountryFlag('BE', [C.black, C.yellow, C.red]), // 🇧🇪
        new CountryFlag('BZ', [C.blue, C.white]), // 🇧🇿
        new CountryFlag('BJ', [C.green, C.yellow, C.red]), // 🇧🇯
        new CountryFlag('BM', ['🇬🇧', C.blue]), // 🇧🇲
        new CountryFlag('BT', [C.orange, C.red, '🐉']), // 🇧🇹
        new CountryFlag('BO', [C.red, C.yellow, C.green]), // 🇧🇴
        new CountryFlag('BQ', [C.yellow, C.white, C.blue]), // 🇧🇶
        new CountryFlag('BA', [C.blue, C.yellow, C.blue]), // 🇧🇦
        new CountryFlag('BW', [C.blue, C.black, C.blue]), // 🇧🇼
        new CountryFlag('BR', [C.green, C.yellow, C.blue]), // 🇧🇷
        new CountryFlag('IO', ['🇬🇧', C.blue, C.white]), // 🇮🇴
        new CountryFlag('BN', [C.yellow, C.white, C.black, C.yellow]), // 🇧🇳
        new CountryFlag('BG', [C.white, C.green, C.red]), // 🇧🇬
        new CountryFlag('BF', [C.red, '★', C.green]), // 🇧🇫
        new CountryFlag('BI', [C.green, C.white, C.red, C.white, C.green, C.white, C.red]), // 🇧🇮
        new CountryFlag('KH', [C.blue, C.red, C.blue]), // 🇰🇭
        new CountryFlag('CM', [C.green, C.red, '★', C.yellow]), // 🇨🇲
        new CountryFlag('CA', [C.red, C.white, '🍁', C.red], [['🍁']]), // 🇨🇦
        new CountryFlag('CV', [C.blue, C.white, C.red, C.blue]), // 🇨🇻
        new CountryFlag('KY', ['🇬🇧', C.blue]), // 🇰🇾
        new CountryFlag('CF', [C.blue, C.white, C.green, C.yellow, C.red]), // 🇨🇫
        new CountryFlag('TD', [C.blue, C.yellow, C.red]), // 🇹🇩
        new CountryFlag('CL', [C.blue, '★', C.white, C.red]), // 🇨🇱
        new CountryFlag('CN', ['★', C.red]), // 🇨🇳
        new CountryFlag('CX', [C.blue, C.green]), // 🇨🇽
        new CountryFlag('CC', [C.green, '🌙', '✨']), // 🇨🇨
        new CountryFlag('CO', [C.yellow, C.blue, C.red]), // 🇨🇴
        new CountryFlag('KM', [C.green, C.yellow, C.white, C.red, C.blue]), // 🇰🇲
        new CountryFlag('CG', [C.green, C.yellow, C.red]), // 🇨🇬
        new CountryFlag('CD', [C.blue, C.yellow, C.red, C.yellow, C.blue]), // 🇨🇩
        new CountryFlag('CK', ['🇬🇧', C.blue]), // 🇨🇰
        new CountryFlag('CR', [C.blue, C.white, C.red, C.white, C.blue]), // 🇨🇷
        new CountryFlag('HR', [C.red, C.white, C.blue]), // 🇭🇷
        new CountryFlag('CU', [C.red, C.blue, C.white]), // 🇨🇺
        new CountryFlag('CW', [C.blue, C.yellow, C.blue]), // 🇨🇼
        new CountryFlag('CY', [C.white, C.yellow]), // 🇨🇾
        new CountryFlag('CZ', [C.blue, C.white, C.red]), // 🇨🇿
        new CountryFlag('CI', [C.orange, C.white, C.green]), // 🇨🇮
        new CountryFlag('DK', [C.red, C.white]), // 🇩🇰
        new CountryFlag('DJ', [C.white, C.blue, C.green]), // 🇩🇯
        new CountryFlag('DM', [C.green, C.black, C.red]), // 🇩🇲
        new CountryFlag('DO', [C.blue, C.red, C.white, C.red, C.blue]), // 🇩🇴
        new CountryFlag('EC', [C.yellow, C.blue, C.red]), // 🇪🇨
        new CountryFlag('EG', [C.red, C.white, C.black]), // 🇪🇬
        new CountryFlag('SV', [C.blue, C.white, C.blue]), // 🇸🇻
        new CountryFlag('GQ', [C.blue, C.green, C.white, C.red]), // 🇬🇶
        new CountryFlag('ER', [C.red, C.green, C.blue]), // 🇪🇷
        new CountryFlag('EE', [C.blue, C.black, C.white]), // 🇪🇪
        new CountryFlag('SZ', [C.blue, C.yellow, C.red, C.yellow, C.blue]), // 🇸🇿
        new CountryFlag('ET', [C.green, C.yellow, C.red]), // 🇪🇹
        new CountryFlag('FK', ['🇬🇧', C.blue]), // 🇫🇰
        new CountryFlag('FO', [C.white, C.blue, C.red]), // 🇫🇴
        new CountryFlag('FJ', ['🇬🇧', C.blue]), // 🇫🇯
        new CountryFlag('FI', [C.white, C.blue]), // 🇫🇮
        new CountryFlag('FR', [C.blue, C.white, C.red]), // 🇫🇷
        new CountryFlag('GF', [C.yellow, '★', C.green]), // 🇬🇫
        new CountryFlag('PF', [C.red, C.white, C.red]), // 🇵🇫
        new CountryFlag('TF', ['🇫🇷', C.blue]), // 🇹🇫
        new CountryFlag('GA', [C.green, C.yellow, C.blue]), // 🇬🇦
        new CountryFlag('GM', [C.red, C.blue, C.green]), // 🇬🇲
        new CountryFlag('GE', [C.white, C.red, C.white]), // 🇬🇪
        new CountryFlag('DE', [C.black, C.red, C.yellow]), // 🇩🇪
        new CountryFlag('GH', [C.red, C.yellow, C.green]), // 🇬🇭
        new CountryFlag('GI', [C.white, C.red]), // 🇬🇮
        new CountryFlag('GR', [C.blue, C.white, C.blue, C.white, C.blue]), // 🇬🇷
        new CountryFlag('GL', [C.white, C.red]), // 🇬🇱
        new CountryFlag('GD', [C.green, C.yellow, C.green, C.yellow]), // 🇬🇩
        new CountryFlag('GP', [C.black, C.blue, C.yellow, C.black]), // 🇬🇵
        new CountryFlag('GU', [C.blue, C.yellow]), // 🇬🇺
        new CountryFlag('GT', [C.blue, C.white, C.blue]), // 🇬🇹
        new CountryFlag('GG', [C.white, C.red, C.yellow]), // 🇬🇬
        new CountryFlag('GN', [C.red, C.yellow, C.green]), // 🇬🇳
        new CountryFlag('GW', [C.red, C.yellow, C.green]), // 🇬🇼
        new CountryFlag('GY', [C.red, C.yellow, C.green]), // 🇬🇾
        new CountryFlag('HT', [C.blue, C.red]), // 🇭🇹
        new CountryFlag('HM', ['🇬🇧', C.blue, '✨']), // 🇭🇲
        new CountryFlag('VA', [C.yellow, C.white]), // 🇻🇦
        new CountryFlag('HN', [C.blue, C.white, C.blue]), // 🇭🇳
        new CountryFlag('HK', [C.red, C.white]), // 🇭🇰
        new CountryFlag('HU', [C.red, C.white, C.green]), // 🇭🇺
        new CountryFlag('IS', [C.blue, C.white, C.red]), // 🇮🇸
        new CountryFlag('IN', [C.orange, C.white, C.green]), // 🇮🇳
        new CountryFlag('ID', [C.red, C.white]), // 🇮🇩
        new CountryFlag('IR', [C.green, C.white, C.red]), // 🇮🇷
        new CountryFlag('IQ', [C.red, C.white, C.black]), // 🇮🇶
        new CountryFlag('IE', [C.green, C.white, C.orange]), // 🇮🇪
        new CountryFlag('IM', [C.red, C.white]), // 🇮🇲
        new CountryFlag('IL', [C.blue, C.white, C.blue]), // 🇮🇱
        new CountryFlag('IT', [C.green, C.white, C.red]), // 🇮🇹
        new CountryFlag('JM', [C.black, C.yellow, C.green, C.yellow, C.black]), // 🇯🇲
        new CountryFlag('JP', [C.white, C.red]), // 🇯🇵
        new CountryFlag('JE', [C.white, C.red]), // 🇯🇪
        new CountryFlag('JO', [C.red, C.black, C.white, C.green]), // 🇯🇴
        new CountryFlag('KZ', [C.blue, C.yellow]), // 🇰🇿
        new CountryFlag('KE', [C.black, C.red, C.green]), // 🇰🇪
        new CountryFlag('KI', [C.red, '🌅', C.white, C.blue]), // 🇰🇮
        new CountryFlag('KP', [C.blue, C.red, '★', C.blue]), // 🇰🇵
        new CountryFlag('KR', [C.white, C.red, C.blue]), // 🇰🇷
        new CountryFlag('XK', [C.blue, C.yellow]), // 🇽🇰
        new CountryFlag('KW', [C.black, C.green, C.white, C.red]), // 🇰🇼
        new CountryFlag('KG', [C.red, C.yellow]), // 🇰🇬
        new CountryFlag('LA', [C.red, C.blue, C.white, C.red]), // 🇱🇦
        new CountryFlag('LV', [C.red, C.white, C.red]), // 🇱🇻
        new CountryFlag('LB', [C.red, C.white, '🌲', C.red]), // 🇱🇧
        new CountryFlag('LS', [C.blue, C.white, C.green]), // 🇱🇸
        new CountryFlag('LR', [C.blue, '★', C.red, C.white, C.red, C.white]), // 🇱🇷
        new CountryFlag('LY', [C.red, C.black, '☪', C.green]), // 🇱🇾
        new CountryFlag('LI', [C.blue, '👑', C.red]), // 🇱🇮
        new CountryFlag('LT', [C.yellow, C.blue, C.red]), // 🇱🇹
        new CountryFlag('LU', [C.red, C.white, C.blue]), // 🇱🇺
        new CountryFlag('MO', [C.green]), // 🇲🇴
        new CountryFlag('MG', [C.white, C.red, C.green]), // 🇲🇬
        new CountryFlag('MW', [C.black, C.red, C.green]), // 🇲🇼
        new CountryFlag('MY', [C.blue, '☪', C.red, C.white, C.red, C.white]), // 🇲🇾
        new CountryFlag('MV', [C.red, C.green, '🌙']), // 🇲🇻
        new CountryFlag('ML', [C.green, C.yellow, C.red]), // 🇲🇱
        new CountryFlag('MT', [C.white, C.red]), // 🇲🇹
        new CountryFlag('MH', [C.blue, C.orange, C.white, C.blue]), // 🇲🇭
        new CountryFlag('MQ', [C.blue, C.white]), // 🇲🇶
        new CountryFlag('MR', [C.red, C.green, '☪', C.red]), // 🇲🇷
        new CountryFlag('MU', [C.red, C.blue, C.yellow, C.green]), // 🇲🇺
        new CountryFlag('YT', ['🇫🇷', C.white]), // 🇾🇹
        new CountryFlag('MX', [C.green, C.white, C.red]), // 🇲🇽
        new CountryFlag('FM', [C.blue, C.white]), // 🇫🇲
        new CountryFlag('MD', [C.blue, C.yellow, C.red]), // 🇲🇩
        new CountryFlag('MC', [C.red, C.white]), // 🇲🇨
        new CountryFlag('MN', [C.red, C.blue, C.red]), // 🇲🇳
        new CountryFlag('ME', [C.red, C.yellow]), // 🇲🇪
        new CountryFlag('MS', ['🇬🇧', C.blue]), // 🇲🇸
        new CountryFlag('MA', [C.red, C.yellow, '☆']), // 🇲🇦
        new CountryFlag('MZ', [C.red, C.green, C.black, C.yellow]), // 🇲🇿
        new CountryFlag('MM', [C.yellow, C.green, '☆', C.red]), // 🇲🇲
        new CountryFlag('NA', [C.blue, C.red, C.green]), // 🇳🇦
        new CountryFlag('NR', [C.blue, C.yellow, C.blue]), // 🇳🇷
        new CountryFlag('NP', [C.blue, C.red, '🌙', C.red, '☀️', C.red, C.blue]), // 🇳🇵
        new CountryFlag('NL', [C.red, C.white, C.blue]), // 🇳🇱
        new CountryFlag('NC', [C.blue, C.red, C.green, C.yellow]), // 🇳🇨
        new CountryFlag('NZ', ['🇬🇧', C.blue, '✨']), // 🇳🇿
        new CountryFlag('NI', [C.blue, C.white, C.blue]), // 🇳🇮
        new CountryFlag('NE', [C.orange, C.white, C.green]), // 🇳🇪
        new CountryFlag('NG', [C.green, C.white, C.green]), // 🇳🇬
        new CountryFlag('NU', ['🇬🇧', C.yellow]), // 🇳🇺
        new CountryFlag('NF', [C.green, C.white, '🌲', C.green]), // 🇳🇫
        new CountryFlag('MK', [C.red, C.yellow, C.red]), // 🇲🇰
        new CountryFlag('MP', [C.blue, '☆', C.blue]), // 🇲🇵
        new CountryFlag('NO', [C.red, C.white, C.blue]), // 🇳🇴
        new CountryFlag('OM', [C.red, C.white, C.red, C.green]), // 🇴🇲
        new CountryFlag('PK', [C.white, C.green, '☪']), // 🇵🇰
        new CountryFlag('PW', [C.blue, C.yellow]), // 🇵🇼
        new CountryFlag('PS', [C.red, C.black, C.white, C.green]), // 🇵🇸
        new CountryFlag('PA', [C.white, '☆', C.red, C.blue, '☆', C.white]), // 🇵🇦
        new CountryFlag('PG', [C.black, '✨', C.red]), // 🇵🇬
        new CountryFlag('PY', [C.red, C.white, C.blue]), // 🇵🇾
        new CountryFlag('PE', [C.red, C.white, C.red]), // 🇵🇪
        new CountryFlag('PH', [C.white, C.blue, C.red]), // 🇵🇭
        new CountryFlag('PN', ['🇬🇧', C.blue]), // 🇵🇳
        new CountryFlag('PL', [C.white, C.red]), // 🇵🇱
        new CountryFlag('PT', [C.green, C.yellow, C.red]), // 🇵🇹
        new CountryFlag('PR', [C.blue, '☆', C.red, C.white, C.red]), // 🇵🇷
        new CountryFlag('QA', [C.white, C.red]), // 🇶🇦
        new CountryFlag('RE', [C.blue, C.red]), // 🇷🇪
        new CountryFlag('RO', [C.blue, C.yellow, C.red]), // 🇷🇴
        new CountryFlag('RU', [C.white, C.blue, C.white], [], 'https://en.wikipedia.org/wiki/White-blue-white_flag'), // 🇷🇺
        new CountryFlag('RW', [C.blue, C.yellow, C.green]), // 🇷🇼
        new CountryFlag('BL', ['🇫🇷', C.white]), // 🇧🇱
        new CountryFlag('SH', ['🇬🇧', C.blue]), // 🇸🇭
        new CountryFlag('KN', [C.green, C.black, C.red]), // 🇰🇳
        new CountryFlag('LC', [C.blue, C.black, C.yellow]), // 🇱🇨
        new CountryFlag('MF', [C.blue, C.white, C.red]), // 🇲🇫
        new CountryFlag('PM', ['🇫🇷', C.blue]), // 🇵🇲
        new CountryFlag('VC', [C.blue, C.yellow, C.green]), // 🇻🇨
        new CountryFlag('WS', [C.blue, C.red]), // 🇼🇸
        new CountryFlag('SM', [C.white, C.blue]), // 🇸🇲
        new CountryFlag('ST', [C.red, C.green, C.yellow, C.green]), // 🇸🇹
        new CountryFlag('SA', [C.green, C.white]), // 🇸🇦
        new CountryFlag('SN', [C.green, C.yellow, '☆', C.red]), // 🇸🇳
        new CountryFlag('RS', [C.red, C.blue, C.white]), // 🇷🇸
        new CountryFlag('SC', [C.blue, C.yellow, C.red, C.white, C.green]), // 🇸🇨
        new CountryFlag('SL', [C.green, C.white, C.blue]), // 🇸🇱
        new CountryFlag('SG', [C.red, '☪', C.white]), // 🇸🇬
        new CountryFlag('SX', [C.white, C.red, C.blue]), // 🇸🇽
        new CountryFlag('SK', [C.white, C.blue, C.red]), // 🇸🇰
        new CountryFlag('SI', [C.white, C.blue, C.red]), // 🇸🇮
        new CountryFlag('SB', [C.blue, C.yellow, C.green]), // 🇸🇧
        new CountryFlag('SO', [C.blue, '☆']), // 🇸🇴
        new CountryFlag('ZA', [C.black, C.green, C.red, C.blue]), // 🇿🇦
        new CountryFlag('GS', ['🇬🇧', C.blue]), // 🇬🇸
        new CountryFlag('SS', [C.blue, C.black, C.red, C.green]), // 🇸🇸
        new CountryFlag('ES', [C.red, C.yellow, C.red]), // 🇪🇸
        new CountryFlag('LK', [C.orange, C.red]), // 🇱🇰
        new CountryFlag('SD', [C.green, C.red, C.white, C.black]), // 🇸🇩
        new CountryFlag('SR', [C.green, C.white, C.red, '☆', C.white, C.green]), // 🇸🇷
        new CountryFlag('SJ', [C.red, C.blue]), // 🇸🇯
        new CountryFlag('SE', [C.blue, C.yellow]), // 🇸🇪
        new CountryFlag('CH', [C.red, C.white, '✚']), // 🇨🇭
        new CountryFlag('SY', [C.red, C.white, '★', '★', C.black]), // 🇸🇾
        new CountryFlag('TW', [C.blue, C.red]), // 🇹🇼
        new CountryFlag('TJ', [C.red, C.white, C.green]), // 🇹🇯
        new CountryFlag('TZ', [C.green, C.black, C.blue]), // 🇹🇿
        new CountryFlag('TH', [C.red, C.white, C.blue, C.white, C.red]), // 🇹🇭
        new CountryFlag('TL', [C.black, '☆', C.yellow, C.red]), // 🇹🇱
        new CountryFlag('TG', [C.red, '☆', C.green, C.yellow, C.green, C.yellow, C.green]), // 🇹🇬
        new CountryFlag('TK', [C.blue, '✨', C.yellow]), // 🇹🇰
        new CountryFlag('TO', [C.white, '✚', C.red]), // 🇹🇴
        new CountryFlag('TT', [C.red, C.black, C.red]), // 🇹🇹
        new CountryFlag('TN', [C.red, C.white, '☪']), // 🇹🇳
        new CountryFlag('TR', [C.red, '☪']), // 🇹🇷
        new CountryFlag('TM', [C.green, '☪']), // 🇹🇲
        new CountryFlag('TC', ['🇬🇧', C.blue]), // 🇹🇨
        new CountryFlag('TV', ['🇬🇧', C.blue]), // 🇹🇻
        new CountryFlag('UG', [C.black, C.yeallo, C.red, C.black, C.yeallo, C.red]), // 🇺🇬
        new CountryFlag('UA', [C.blue, C.yellow]), // 🇺🇦
        new CountryFlag('AE', [C.red, C.green, C.white, C.black]), // 🇦🇪
        new CountryFlag('GB', [C.blue, C.white, C.red, C.white, C.blue]), // 🇬🇧
        new CountryFlag('US', [C.blue, '☆', C.red, C.white, C.red, C.white]), // 🇺🇸
        new CountryFlag('UY', [C.white, C.blue, C.white, C.blue, C.white, C.blue]), // 🇺🇾
        new CountryFlag('UZ', [C.blue, C.white, C.green]), // 🇺🇿
        new CountryFlag('VU', [C.black, C.red, C.green]), // 🇻🇺
        new CountryFlag('VE', [C.yellow, C.blue, C.red]), // 🇻🇪
        new CountryFlag('VN', [C.red, C.yellow, '☆']), // 🇻🇳
        new CountryFlag('VG', ['🇬🇧', C.blue]), // 🇻🇬
        new CountryFlag('VI', [C.white, C.yellow]), // 🇻🇮
        new CountryFlag('WF', ['🇫🇷', C.red]), // 🇼🇫
        new CountryFlag('EH', [C.red, C.black, C.white, C.green]), // 🇪🇭
        new CountryFlag('YE', [C.red, C.white, C.black]), // 🇾🇪
        new CountryFlag('ZM', [C.green, C.red]), // 🇿🇲
        new CountryFlag('ZW', [C.white, C.green, C.yellow, C.red, C.black, C.red, C.yellow, C.green]), // 🇿🇼
    ]),
    keyboard: null,
}
